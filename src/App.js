import './styles/style.css';
import Header from './components/Header';
import CharacterGrid from './components/CharacterGrid';
import { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Quote from './components/Quote';

function App() {
    const [list, setList] = useState([]);

    useEffect(() => {
        const abortCont = new AbortController();
        fetchList(abortCont);
        return () => abortCont.abort();
    }, []);

    const fetchList = async (abortCont) => {
        const res = await fetch('https://breakingbadapi.com/api/characters', {signal: abortCont.signal});
        const data = await res.json();
        setList(data);
    }

    return (
        <Router>
            <div className="container-app">
                <Header />
                <Switch>
                    <Route exact path="/" >
                        <CharacterGrid list={list} />
                    </Route>
                    <Route path="/quotes" >
                        <Quote/> 
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}

export default App;
