const Character = ({ character }) => {

    return (
        <div className="card">
            <img className="card__img" src={character.img} alt="card__img"></img>
            <div className="card__text">
                <ul>
                    <li><h3>Name: </h3>{character.name}</li>
                    <li><h3>Actor: </h3>{character.portrayed}</li>
                    <li><h3>Birthday: </h3>{character.birthday}</li>
                    <li><h3>Nickname: </h3>{character.nickname}</li>
                    <li><h3>Status: </h3>{character.status}</li>
                </ul>
            </div>
        </div>
    );
}

export default Character;