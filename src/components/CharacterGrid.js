import Character from "./Character";

const CharacterGrid = ({ list }) => {

    return (
        <div className="charactergrid">
            {list.map((character, index) => <Character key={index} character={character}/>)}
        </div>
    );
}

export default CharacterGrid;
