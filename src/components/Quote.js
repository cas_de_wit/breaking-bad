import { useState, useEffect } from 'react';

const Quote = () => {
    const [quote, setQuote] = useState([{
        quote_id: 0,
        quote: "",
        author: "",
        series: ""
        }]);

    useEffect(() => {
        const abortCont = new AbortController();
        fetchQuote(abortCont);
        return () => abortCont.abort();
    }, []);

    const fetchQuote = async (abortCont) => {
        const res = await fetch('https://breakingbadapi.com/api/quote/random', {signal: abortCont.signal});
        const data = await res.json();
        setQuote(data);
    }
    return (
        <div className="quote">
            <h1 className="quote__text"><em>{quote[0].quote}</em></h1>
            <h3 className="quote__author">- {quote[0].author} -</h3>
            <button onClick={fetchQuote} className="quote__btn-quote">Next quote</button>
        </div>
    );
}

export default Quote;
