import logo from '../images/logo.png';
import { Link } from 'react-router-dom';

const Header = () => {

    return (
        <div className="container-header">
            <img src={logo} alt=""></img>
            <div className="links">
                <Link to="/" className="links__link">Characters</Link>
                <Link to="/quotes" className="links__link">Quotes</Link>
            </div>
        </div>
    );
}

export default Header;